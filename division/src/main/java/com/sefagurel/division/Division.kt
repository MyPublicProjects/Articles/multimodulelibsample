package com.sefagurel.division

fun division(num1: Int, num2: Int): Float {
    return num1.toFloat() / num2.toFloat()
}