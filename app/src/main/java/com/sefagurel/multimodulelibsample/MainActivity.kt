package com.sefagurel.multimodulelibsample

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.sefagurel.multimodulelibsample.ui.theme.MultiModuleLibSampleTheme
import com.sefagurel.division.division
import com.sefagurel.minus.minus
import com.sefagurel.multiply.multiply
import com.sefagurel.plus.plus

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MultiModuleLibSampleTheme {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(64.dp),
                    verticalArrangement = Arrangement.spacedBy(32.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(text = "3 + 3 = ${plus(3, 3)}")
                    Text(text = "3 - 3 = ${minus(3, 3)}")
                    Text(text = "3 * 3 = ${multiply(3, 3)}")
                    Text(text = "3 / 3 = ${division(3, 3)}")
                }
            }
        }
    }
}