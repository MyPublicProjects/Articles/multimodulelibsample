pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}

rootProject.name = "MultiModuleLibSample"
include(":app")
include(":plus")
include(":minus")
include(":multiply")
include(":division")