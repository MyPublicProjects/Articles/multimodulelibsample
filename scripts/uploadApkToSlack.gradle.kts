tasks.register("uploadApkToSlack") {
    val slackAuthToken = project.extra["slack_auth_token"]
    val slackChannelId = project.extra["slack_channel_id"]

    val apkFolder = "${project.buildDir}/outputs/apk/"

    val apkFile = File(apkFolder).walkTopDown().find { it.name.contains(".apk") }

    doLast {
        if (apkFile != null) {
            exec {
                commandLine = listOf(
                    "curl",
                    "-F",
                    "initial_comment= Test message",
                    "-F",
                    "file=@${apkFile.path}",
                    "-F",
                    "channels=$slackChannelId",
                    "-H",
                    "Authorization:Bearer $slackAuthToken",
                    "https://slack.com/api/files.upload"
                )
            }
        }
    }
}