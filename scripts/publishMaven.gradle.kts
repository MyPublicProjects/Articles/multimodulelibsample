allprojects {
    repositories {
        mavenAuthentication()
    }
}

fun RepositoryHandler.mavenAuthentication() {
    val artifactoryUrl: String by project
    val artifactoryUsername: String by project
    val artifactoryPassword: String by project

    maven {
        url = uri(artifactoryUrl)
        credentials(PasswordCredentials::class) {
            username = artifactoryUsername
            password = artifactoryPassword
        }
        authentication {
            create<BasicAuthentication>("basic")
        }
    }
}

val publishModules = setOf(
    project(":plus"),
    project(":minus"),
    project(":multiply"),
    project(":division"),
)

configure(publishModules) {
    apply(plugin = "maven-publish")

    val libVersion: String by project
    val moduleName = name

    afterEvaluate {
        configure<PublishingExtension> {
            publications {
                create<MavenPublication>("release") {
                    groupId = "com.sefagurel"
                    artifactId = moduleName
                    version = libVersion
                    afterEvaluate {
                        from(components["release"])
                    }
                }
            }
            repositories {
                mavenAuthentication()
            }
        }
    }
}